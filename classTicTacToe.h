#include <iostream>
#include <array>
#include <string>

const size_t boardRows = 3;
const size_t boardColumns = 3;

class TicTacToe
{
public:
	enum Piece { X, O, BLANK, CAT };
	void play(int row, int column, Piece p);
	bool isGameOver();
	Piece turn();
	Piece whoWon(int);
	Piece pieceAt(int row, int column);
	void resetGame();
private:
	std::array < std::array< Piece, boardColumns >, boardRows> boardPlacement;
};

void commandLoop();
void printHelp();
char displayPiece(int, int, TicTacToe*);
std::string displayWinner(int);
