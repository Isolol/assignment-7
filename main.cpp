#include <iostream>
#include <string>
#include "classTicTacToe.h"

int main() 
{
	commandLoop();
	return 0;
}

void commandLoop()
{
	TicTacToe TTTGame;
	TTTGame.resetGame();
	int loopController = 0;

	while(loopController != 1)
	{
		bool gameState = TTTGame.isGameOver();
		
		std::cout << "What command would you like to execute? (Press 'h' for help)" << std::endl;
		char inputCommand = ' ';
		std::cin >> inputCommand;

		if(inputCommand == 'h')
		{
			printHelp();
		}
		if(inputCommand == 't')
		{
			TTTGame.turn();
		}
		if(inputCommand == 'p')
		{
			int inputColumn = 0;
			int inputRow = 0;

			std::cout << std::endl << "What row would you like to place your piece?" << std::endl;
			std::cin >> inputRow;
			std::cout << "What column would you like to place your piece?" << std::endl;
			std::cin >> inputColumn;

			TTTGame.play(inputRow, inputColumn, TTTGame.turn());
		}
		if(inputCommand == 'q')
		{
			loopController = 1;
			break;
		}
		if(inputCommand == 'r')
		{
			TTTGame.resetGame();
		}
		if(inputCommand == 'd')
		{
			std::cout << " " << displayPiece(0,0, &TTTGame) << " * " << displayPiece(0,1, &TTTGame) << " * " << displayPiece(0,2, &TTTGame) << std::endl
				<< "***********" << std::endl
				<< " " << displayPiece(1,0, &TTTGame) << " * " << displayPiece(1,1, &TTTGame) << " * " << displayPiece(1,2, &TTTGame) << std::endl
				<< "***********" << std::endl
				<< " " << displayPiece(2,0, &TTTGame) << " * " << displayPiece(2,1, &TTTGame) << " * " << displayPiece(2,2, &TTTGame) << std::endl;
		}
		if(gameState)
		{
			std::cout << "The game is over and " << displayWinner(TTTGame.whoWon(0)) << " is the winner! Game is resetting." << std::endl;
			TTTGame.resetGame();
		}
	}
}

void printHelp()
{
	std::cout << "'h' - Help (This printout)" << std::endl
		<< "'p' - Play your turn" << std::endl
		<< "'d' - Display the current board" << std::endl
		<< "'t' - Displays whose turn it is" << std::endl
		<< "'q' - Quits the game" << std::endl
		<< "'r' - Reset the game" << std::endl;
}

bool TicTacToe::isGameOver()
{
	int blankSlot = 0;
	for(int i = 0; i < 3; i++)
	{
		for(int ii = 0; ii < 3; ii++)
		{
			if(boardPlacement[i][ii] == 2)
			{
				blankSlot++;
			}
		}
	}
	if(blankSlot >= 1)
	{
		return 0;
	}
	whoWon(blankSlot);
}

TicTacToe::Piece TicTacToe::pieceAt(int row, int column)
{
	return boardPlacement[row][column];
}

TicTacToe::Piece TicTacToe::turn()
{
	int amountOfX = 0;
	int amountOfO = 0;
	for(int i = 0; i < 3; i++)
	{
		for(int ii = 0; ii < 3; ii++)
		{
			if(boardPlacement[i][ii] == 0)
			{
				amountOfX++;
			}
			if(boardPlacement[i][ii] == 1)
			{
				amountOfO++;
			}
		}
	}
	if(amountOfX > amountOfO)
	{
		return O;
	}
	else
	{
		return X;
	}
}

void TicTacToe::play(int row, int column, TicTacToe::Piece pieceTurn)
{
	boardPlacement[row][column] = pieceTurn;
}

char displayPiece(int row, int column, TicTacToe *TTTGame)
{
	int someReturnValue = TTTGame->pieceAt(row, column);

	if(someReturnValue == 0)
	{
		return 'X';
	}
	if(someReturnValue == 1)
	{
		return 'O';
	}
	else
	{
		return ' ';
	}
}
TicTacToe::Piece TicTacToe::whoWon(int blankSlot)
{
	if(boardPlacement[0][0] == boardPlacement[1][1] == boardPlacement[2][2] || boardPlacement[0][2] == boardPlacement[1][1] == boardPlacement[2][0])
	{
		return boardPlacement[1][1];
	}
	if(boardPlacement[0][0] == boardPlacement[0][1] == boardPlacement[0][2])
	{
		return boardPlacement[0][0];
	}
	if(boardPlacement[1][0] == boardPlacement[1][1] == boardPlacement[1][2])
	{
		return boardPlacement[1][1];
	}
	if(boardPlacement[2][0] == boardPlacement[2][1] == boardPlacement[2][2])
	{
		return boardPlacement[2][2];
	}
	if(boardPlacement[0][0] == boardPlacement[1][0] == boardPlacement[2][0])
	{
		return boardPlacement[1][0];
	}
	if(boardPlacement[0][1] == boardPlacement[1][1] == boardPlacement[2][1])
	{
		return boardPlacement[1][1];
	}
	if(boardPlacement[0][2] == boardPlacement[1][2] == boardPlacement[2][2])
	{
		return boardPlacement[2][2];
	}
	if(blankSlot < 1)
	{
		return CAT;
	}
}
void TicTacToe::resetGame()
{
	for(int i = 0; i < 3; i++)
	{
		for(int ii = 0; ii < 3; ii++)
		{
			boardPlacement[i][ii] = BLANK;
		}
	}
}
std::string displayWinner(int finalPiece)
{
	if(finalPiece == 0)
	{
		return "X";
	}
	if(finalPiece == 1)
	{
		return "O";
	}
	if(finalPiece == 3)
	{
		return "CAT";
	}
	else
	{
		return "";
	}
}
